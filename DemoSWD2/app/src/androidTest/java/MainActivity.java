//import android.support.v7.app.AppCompatActivity;
//import android.os.Bundle;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//
//import com.android.volley.Request;
//import com.android.volley.RequestQueue;
//import com.android.volley.Response;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.StringRequest;
//import com.android.volley.toolbox.Volley;
//import com.example.dell.demoswd2.R;
//import com.example.dell.demoswd2.promotions.PromotionAdapter;
//import com.example.dell.demoswd2.promotions.Promotions;
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class MainActivity extends AppCompatActivity {
//    final String URL_GET_DATA = "http://202.78.227.93:7777/api/promotions";
//    RecyclerView recyclerView;
//    PromotionAdapter adapter;
//    List<Promotions> promotionsList;
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//
//        recyclerView = findViewById(R.id.recyclerView);
//        recyclerView.setHasFixedSize(true);
//        recyclerView.setLayoutManager(new LinearLayoutManager(this));
//
//        promotionsList = new ArrayList<>();
//
//        loadPromotion();
//    }
//
//    public void loadPromotion() {
//        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_GET_DATA,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        try {
//                            JSONObject jsonObject = new JSONObject(response);
//                            JSONArray jsonArray = jsonObject.getJSONArray("data");
////                            JSONArray jsonArray = new JSONArray(response);
//
//                            for (int i = 0; i < jsonArray.length(); i++) {
//                                JSONObject obj = jsonArray.getJSONObject(i);
//
//                                Promotions promotions = new Promotions(
//                                        obj.getString("code"),
//                                        obj.getString("name"),
//                                        obj.getString("description")
//                                );
//
//                                promotionsList.add(promotions);
//                            }
//
//                            adapter = new PromotionAdapter(promotionsList, getApplicationContext());
//                            recyclerView.setAdapter(adapter);
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//
//                    }
//                });
//        RequestQueue requestQueue = Volley.newRequestQueue(this);
//        requestQueue.add(stringRequest);
//    }
//}
// TEST
// TEST2
// TEST3
// TEST4 - N Duy
// TEST5 - M Duy