package com.example.dell.demoswd2.promotions.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.dell.demoswd2.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PromotionDetailsDetail extends AppCompatActivity {
    private TextView code, description, minAge, maxAge, dayOfWeek;
    private CheckBox gender;
    private RequestQueue mQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_3);

        code = findViewById(R.id.txtCode);
        description = findViewById(R.id.txtDescription);
        minAge = findViewById(R.id.txtMinAge);
        maxAge = findViewById(R.id.txtMaxAge);
        dayOfWeek = findViewById(R.id.txtDayOfWeek);
        gender = findViewById(R.id.cbGender);

        Intent intent = getIntent();
        String promotion_detail_id = intent.getStringExtra("promotion_detail_id");


        jsonParse(promotion_detail_id);
    }

    private void jsonParse(String promotionDetailId) {

        mQueue = Volley.newRequestQueue(this);

        String url = "http://35.240.180.181:8888/api/promotion_details?active=true&ids=";

        url = url + promotionDetailId;
        Toast.makeText(this, " " + url, Toast.LENGTH_SHORT).show();
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray jsonArray = response.getJSONArray("data");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject promotionDetail = jsonArray.getJSONObject(i);

                        String promotion_detail_code = promotionDetail.getString("code");
                        String promotion_detail_description = promotionDetail.getString("description");
                        boolean isMale = promotionDetail.getBoolean("gender");
                        String min_age = promotionDetail.getString("min_age");
                        String max_age = promotionDetail.getString("max_age");

                        code.setText(promotion_detail_code);
                        description.setText(promotion_detail_description);
                        minAge.setText(min_age);
                        maxAge.setText(max_age);
                        if (isMale) {
                            gender.setChecked(true);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        mQueue.add(request);
    }
}
