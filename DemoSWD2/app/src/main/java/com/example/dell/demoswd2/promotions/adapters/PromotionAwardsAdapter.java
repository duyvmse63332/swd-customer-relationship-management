package com.example.dell.demoswd2.promotions.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.dell.demoswd2.R;
import com.example.dell.demoswd2.promotions.models.PromotionAwards;

import java.util.List;

public class PromotionAwardsAdapter extends RecyclerView.Adapter<PromotionAwardsAdapter.PromotionAwardsViewHolder> {

    List<PromotionAwards> promotionAwardsList;
    private Context context;

    public PromotionAwardsAdapter(List<PromotionAwards> promotionAwardsList, Context context) {
        this.promotionAwardsList = promotionAwardsList;
        this.context = context;
    }

    class PromotionAwardsViewHolder extends RecyclerView.ViewHolder {
        TextView txtCode;
        CheckBox cbDiscount, cbGift;
        TextView txtDisForProductCode, txtDisAmount, txtDisPercent, txtDisShip;
        TextView txtGiftForProductCode, txtGiftVoucherForPromotion, txtGiftVoucherQuantity, txtGiftProductCode, txtGiftProductQuantity;

        PromotionAwardsViewHolder(View itemView) {
            super(itemView);

            txtCode = itemView.findViewById(R.id.txtCode);
            cbDiscount = itemView.findViewById(R.id.cbIsDiscount);
            cbGift = itemView.findViewById(R.id.cbIsGift);
            txtDisForProductCode = itemView.findViewById(R.id.txtDisForProductCode);
            txtDisAmount = itemView.findViewById(R.id.txtDisAmount);
            txtDisPercent = itemView.findViewById(R.id.txtDisPercent);
            txtDisShip = itemView.findViewById(R.id.txtDisShip);
            txtGiftForProductCode = itemView.findViewById(R.id.txtGiftForProductCode);
            txtGiftVoucherForPromotion = itemView.findViewById(R.id.txtVoucherForPromotionCode);
            txtGiftVoucherQuantity = itemView.findViewById(R.id.txtGiftVoucherQuantity);
            txtGiftProductCode = itemView.findViewById(R.id.txtGiftProductCode);
            txtGiftProductQuantity = itemView.findViewById(R.id.txtGiftProductQuantity);
        }
    }

    @Override
    public PromotionAwardsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.promotion_award_layout, parent, false);
        return new PromotionAwardsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final PromotionAwardsViewHolder holder, final int posistion) {
        final PromotionAwards promotionAwards = promotionAwardsList.get(posistion);
        holder.txtCode.setText(promotionAwards.getCode());
        holder.cbGift.setChecked(promotionAwards.isGift());
        holder.cbDiscount.setChecked(promotionAwards.isDiscount());
        holder.txtDisForProductCode.setText(promotionAwards.getDisForProductCode());
        holder.txtDisAmount.setText(promotionAwards.getDisAmount());
        holder.txtDisPercent.setText(promotionAwards.getDisPercent());
        holder.txtDisShip.setText(promotionAwards.getDisShip());
        holder.txtGiftForProductCode.setText(promotionAwards.getDisForProductCode());
        holder.txtGiftVoucherForPromotion.setText(promotionAwards.getGiftVoucherOfPromotionCode());
        holder.txtGiftVoucherQuantity.setText(promotionAwards.getGiftVoucherQuantity());
        holder.txtGiftProductCode.setText(promotionAwards.getGiftProductCode());
        holder.txtGiftProductQuantity.setText(promotionAwards.getGiftProductQuantity());

    }

    @Override
    public int getItemCount() {
        return promotionAwardsList.size();
    }
}
