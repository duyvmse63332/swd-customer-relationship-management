package com.example.dell.demoswd2.promotions.models;

public class PromotionAwards {
    private int id;
    private String code;
    private boolean isDiscount, isGift;
    private String disForProductCode, disAmount, disPercent, disShip;
    private String giftForProductCode, giftVoucherOfPromotionCode, giftVoucherQuantity, giftProductCode, giftProductQuantity;
    private boolean status;

    public PromotionAwards() {
    }

    public PromotionAwards(int id, String code, boolean isDiscount, boolean isGift, String disForProductCode, String disAmount, String disPercent, String disShip, String giftForProductCode, String giftVoucherOfPromotionCode, String giftVoucherQuantity, String giftProductCode, String giftProductQuantity) {
        this.id = id;
        this.code = code;
        this.isDiscount = isDiscount;
        this.isGift = isGift;
        this.disForProductCode = disForProductCode;
        this.disAmount = disAmount;
        this.disPercent = disPercent;
        this.disShip = disShip;
        this.giftForProductCode = giftForProductCode;
        this.giftVoucherOfPromotionCode = giftVoucherOfPromotionCode;
        this.giftVoucherQuantity = giftVoucherQuantity;
        this.giftProductCode = giftProductCode;
        this.giftProductQuantity = giftProductQuantity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isDiscount() {
        return isDiscount;
    }

    public void setDiscount(boolean discount) {
        isDiscount = discount;
    }

    public boolean isGift() {
        return isGift;
    }

    public void setGift(boolean gift) {
        isGift = gift;
    }

    public String getDisForProductCode() {
        return disForProductCode;
    }

    public void setDisForProductCode(String disForProductCode) {
        this.disForProductCode = disForProductCode;
    }

    public String getDisAmount() {
        return disAmount;
    }

    public void setDisAmount(String disAmount) {
        this.disAmount = disAmount;
    }

    public String getDisPercent() {
        return disPercent;
    }

    public void setDisPercent(String disPercent) {
        this.disPercent = disPercent;
    }

    public String getDisShip() {
        return disShip;
    }

    public void setDisShip(String disShip) {
        this.disShip = disShip;
    }

    public String getGiftForProductCode() {
        return giftForProductCode;
    }

    public void setGiftForProductCode(String giftForProductCode) {
        this.giftForProductCode = giftForProductCode;
    }

    public String getGiftVoucherOfPromotionCode() {
        return giftVoucherOfPromotionCode;
    }

    public void setGiftVoucherOfPromotionCode(String giftVoucherOfPromotionCode) {
        this.giftVoucherOfPromotionCode = giftVoucherOfPromotionCode;
    }

    public String getGiftVoucherQuantity() {
        return giftVoucherQuantity;
    }

    public void setGiftVoucherQuantity(String giftVoucherQuantity) {
        this.giftVoucherQuantity = giftVoucherQuantity;
    }

    public String getGiftProductCode() {
        return giftProductCode;
    }

    public void setGiftProductCode(String giftProductCode) {
        this.giftProductCode = giftProductCode;
    }

    public String getGiftProductQuantity() {
        return giftProductQuantity;
    }

    public void setGiftProductQuantity(String giftProductQuantity) {
        this.giftProductQuantity = giftProductQuantity;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
