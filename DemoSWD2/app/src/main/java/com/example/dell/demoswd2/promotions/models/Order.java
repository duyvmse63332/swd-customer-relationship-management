package com.example.dell.demoswd2.promotions.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Order implements Serializable {
    @SerializedName("RentID")
    private String rentId;
    @SerializedName("OrderDetails")
    private List<OrderDetail> orderDetails;

    public Order(String rentId, List<OrderDetail> orderDetails) {
        this.rentId = rentId;
        this.orderDetails = orderDetails;
    }

    public String getRentId() {
        return rentId;
    }

    public void setRentId(String rentId) {
        this.rentId = rentId;
    }

    public List<OrderDetail> getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(List<OrderDetail> orderDetails) {
        this.orderDetails = orderDetails;
    }
}
