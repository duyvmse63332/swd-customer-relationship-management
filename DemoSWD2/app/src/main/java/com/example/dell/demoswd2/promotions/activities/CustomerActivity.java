package com.example.dell.demoswd2.promotions.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.example.dell.demoswd2.R;


public class CustomerActivity extends AppCompatActivity {
    private TextView customerName, customerAddress, customerEmail, customerDistrict, customerPhone;
    private RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer);

        Intent intent = getIntent();
        String customerId = intent.getStringExtra("customerId");

        //Set các biến
        customerName = findViewById(R.id.txtCustomerName);
        customerAddress = findViewById(R.id.txtCustomerAddress);
        customerEmail = findViewById(R.id.txtCustomerEmail);
        customerDistrict = findViewById(R.id.txtCustomerDistrict);
        customerPhone = findViewById(R.id.txtCustomerPhone);

        customerName.setText(customerId);

        //getCustomerInfor(customerId);
    }

    private void getCustomerInfor(String customerId) {

    }
}
