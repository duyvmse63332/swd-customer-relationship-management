package com.example.dell.demoswd2.promotions.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.dell.demoswd2.R;

public class OrderDetailActivity extends AppCompatActivity {

    private TextView txtOrderId;
    private String orderId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);

        initData();
    }

    private void initData(){
        txtOrderId = findViewById(R.id.txt_order_id);
        Intent intent = getIntent();
        orderId = intent.getStringExtra("id");
        txtOrderId.setText(orderId);
    }
}
