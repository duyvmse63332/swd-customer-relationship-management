package com.example.dell.demoswd2.promotions.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.dell.demoswd2.R;
import com.example.dell.demoswd2.promotions.activities.PromotionDetailsDetail;
import com.example.dell.demoswd2.promotions.activities.PromotionDetailsList;
import com.example.dell.demoswd2.promotions.models.PromotionDetails;

import java.util.List;

public class PromotionDetailsAdapter extends RecyclerView.Adapter<PromotionDetailsAdapter.PromotionDetailViewHolder> {

    List<PromotionDetails> promotionDetailsList;
    private Context context;

    public PromotionDetailsAdapter(List<PromotionDetails> promotionDetailsList, Context context) {
        this.promotionDetailsList = promotionDetailsList;
        this.context = context;
    }

    class PromotionDetailViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener, View.OnClickListener {
        TextView txtCode, txtDescription;

        private PromotionDetailsList.ItemClickListener itemClickListener;

        PromotionDetailViewHolder(View itemView) {
            super(itemView);

            txtCode = itemView.findViewById(R.id.txtCode);
            txtDescription = itemView.findViewById(R.id.txtDescription);

            itemView.setOnLongClickListener(this);
            itemView.setOnClickListener(this);
        }

        public void setItemClickListener(PromotionDetailsList.ItemClickListener itemClickListener) {
            this.itemClickListener = itemClickListener;
        }

        @Override
        public void onClick(View v) {
            itemClickListener.onClick(v, getAdapterPosition(), false); //Set false vi day la long click
        }

        @Override
        public boolean onLongClick(View v) {
            itemClickListener.onClick(v, getAdapterPosition(), true);
            return true;
        }
    }

    @Override
    public PromotionDetailViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.promotion_detail_list_layout, parent, false);
        return new PromotionDetailViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final PromotionDetailViewHolder holder, final int position) {
        final PromotionDetails promotionDetails = promotionDetailsList.get(position);
        holder.txtCode.setText(promotionDetails.getCode());
        holder.txtDescription.setText(promotionDetails.getDescription());

        holder.setItemClickListener(new PromotionDetailsList.ItemClickListener() {
            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                if (isLongClick) {
                    context = view.getContext();
                    Intent intent = new Intent(context, PromotionDetailsDetail.class);
                    //Lưu ý phải thêm vào dấu "" nếu muốn intent truyền được id
                    intent.putExtra("promotion_detail_id", "" + promotionDetails.getId());
                    context.startActivity(intent);
                } else {
                    context = view.getContext();
                    Intent intent = new Intent(context, PromotionDetailsDetail.class);
                    //Lưu ý phải thêm vào dấu "" nếu muốn intent truyền được id
                    intent.putExtra("promotion_detail_id", "" + promotionDetails.getId());
                    context.startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return promotionDetailsList.size();
    }
}
