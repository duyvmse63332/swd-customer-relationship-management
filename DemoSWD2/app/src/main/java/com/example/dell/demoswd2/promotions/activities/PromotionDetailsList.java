package com.example.dell.demoswd2.promotions.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.dell.demoswd2.R;
import com.example.dell.demoswd2.promotions.adapters.PromotionDetailsAdapter;
import com.example.dell.demoswd2.promotions.models.PromotionDetails;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class PromotionDetailsList extends AppCompatActivity {
//    String URL_GET_DATA = "http://apipromo.unicode.edu.vn/api/promotion_details?active=false&of_promotion_id=";
    RecyclerView recyclerView;
    PromotionDetailsAdapter adapter;
    List<PromotionDetails> promotionDetailsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_2);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        promotionDetailsList = new ArrayList<>();

        Intent intent = getIntent();
        String promotion_id = intent.getStringExtra("promotion_id");
//
//        String promotion_id = "6";
//        URL_GET_DATA = URL_GET_DATA + promotion_id;
        String current_request = "http://35.240.180.181:8888/api/promotion_details?active=false&of_promotion_id=" + promotion_id;

        Toast.makeText(this, "URL: " + current_request, Toast.LENGTH_SHORT).show();
        loadPromotionDetail(current_request);

    }

    public void loadPromotionDetail(String current_request) {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, current_request,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);

                                PromotionDetails promotionDetails = new PromotionDetails(
                                        obj.getInt("id"),
                                        obj.getString("code"),
                                        obj.getString("description")
                                );

                                promotionDetailsList.add(promotionDetails);
                            }

                            adapter = new PromotionDetailsAdapter(promotionDetailsList, getApplicationContext());
                            recyclerView.setAdapter(adapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public interface ItemClickListener {
        void onClick(View view, int position, boolean isLongClick);
    }

}
