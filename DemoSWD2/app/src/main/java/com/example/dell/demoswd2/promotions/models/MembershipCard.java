package com.example.dell.demoswd2.promotions.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class MembershipCard implements Serializable {
    @SerializedName("Accounts")
    private List<Account> listAccount;

    public MembershipCard() {
    }

    public List<Account> getListAccount() {
        return listAccount;
    }

    public void setListAccount(List<Account> listAccount) {
        this.listAccount = listAccount;
    }
}
