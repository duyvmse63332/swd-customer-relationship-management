package com.example.dell.demoswd2.promotions.models;

import java.util.Date;

public class Promotions {
    private int id;
    private String name, code, description, image_url;
    private Date from_date, to_date;
    private String daysOfWeek;
    private boolean status;

    public Promotions(int id, String name, String code, String description, String image_url, Date from_date, Date to_date, String daysOfWeek, boolean status) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.description = description;
        this.image_url = image_url;
        this.from_date = from_date;
        this.to_date = to_date;
        this.daysOfWeek = daysOfWeek;
        this.status = status;
    }
    public Promotions(){

    }

    public Promotions(int id, String name, String code, String description, Date from_date, Date to_date, boolean status) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.description = description;
        this.from_date = from_date;
        this.to_date = to_date;
        this.status = status;
    }

    public Promotions(String name, String code, String description) {
        this.name = name;
        this.code = code;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getFrom_date() {
        return from_date;
    }

    public void setFrom_date(Date from_date) {
        this.from_date = from_date;
    }

    public Date getTo_date() {
        return to_date;
    }

    public void setTo_date(Date to_date) {
        this.to_date = to_date;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getDaysOfWeek() {
        return daysOfWeek;
    }

    public void setDaysOfWeek(String daysOfWeek) {
        this.daysOfWeek = daysOfWeek;
    }
}
