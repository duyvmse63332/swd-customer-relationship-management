package com.example.dell.demoswd2.promotions.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Customer implements Serializable {
    @SerializedName("CustomerID")
    private String id;
    @SerializedName("CustomerName")
    private String name;
    @SerializedName("Membership")
    private List<MembershipCard> listMembership;

    public Customer(String id, String name, List<MembershipCard> listMembership) {
        this.id = id;
        this.name = name;
        this.listMembership = listMembership;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<MembershipCard> getListMembership() {
        return listMembership;
    }

    public void setListMembership(List<MembershipCard> listMembership) {
        this.listMembership = listMembership;
    }
}
