package com.example.dell.demoswd2.promotions.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.dell.demoswd2.R;

import com.example.dell.demoswd2.promotions.activities.PromotionActivity;
import com.example.dell.demoswd2.promotions.activities.PromotionAwardsList;
import com.example.dell.demoswd2.promotions.activities.PromotionDetailsList;
import com.example.dell.demoswd2.promotions.models.Promotions;

import java.util.ArrayList;
import java.util.List;

public class PromotionAdapter extends RecyclerView.Adapter<PromotionAdapter.PromotionViewHolder> {

    private List<Promotions> promotionsList;
    private Context context;
    private List<Promotions> originalList;
//    private static int currentPosition = 0;

    public PromotionAdapter(List<Promotions> promotionsList, Context context) {
        this.promotionsList = promotionsList;
        this.context = context;
        this.originalList = promotionsList;
    }

    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                if (constraint == null || constraint.length() == 0) {
                    promotionsList = originalList;
                    results.values = promotionsList;
                    results.count = promotionsList.size();


                } else {
                    ArrayList<Promotions> fRecords = new ArrayList<Promotions>();
                    for (Promotions s : promotionsList) {
                        if (s.getCode().trim().contains(constraint.toString().toUpperCase().trim())) {
                            fRecords.add(s);
                        }
                    }
                    results.values = fRecords;
                    results.count = fRecords.size();
                }
                return results;

            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                promotionsList = (List<Promotions>) results.values;
                notifyDataSetChanged();
            }
        };
        return filter;
    }

    class PromotionViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        TextView txtName, txtCode, txtDescription, txtId, txtDaysOfWeek;
        TextView txtStartDate, txtEndDate;
        ImageView image_Url;
        Button btnReward;

        private PromotionActivity.ItemClickListener itemClickListener;

        PromotionViewHolder(View itemView) {
            super(itemView);

            txtCode = itemView.findViewById(R.id.txtCode);
            txtName = itemView.findViewById(R.id.txtName);
            txtDescription = itemView.findViewById(R.id.txtDescription);
            txtStartDate = itemView.findViewById(R.id.txtBeginDate);
            txtEndDate = itemView.findViewById(R.id.txtEndDate);
            image_Url = itemView.findViewById(R.id.promotionImage);
            btnReward = itemView.findViewById(R.id.btnRewards);
            txtDaysOfWeek = itemView.findViewById(R.id.txtDaysOfWeek);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        //Tạo setter cho biến itemClickListenenr
        public void setItemClickListener(PromotionActivity.ItemClickListener itemClickListener) {
            this.itemClickListener = itemClickListener;
        }

        @Override
        public void onClick(View v) {
            itemClickListener.onClick(v, getAdapterPosition(), false); // Gọi interface , false là vì đây là onClick

        }

        @Override
        public boolean onLongClick(View v) {
            itemClickListener.onClick(v, getAdapterPosition(), true); // Gọi interface , true là vì đây là onLongClick
            return true;
        }
    }

    @NonNull
    @Override
    public PromotionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.promotion_layout, parent, false);
        return new PromotionViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final PromotionViewHolder holder, final int position) {
        final Promotions promotions = promotionsList.get(position);
        holder.txtCode.setText(promotions.getCode());
        holder.txtName.setText(promotions.getName());
        holder.txtDescription.setText(promotions.getDescription());
        holder.txtDaysOfWeek.setText(promotions.getDaysOfWeek());
        holder.txtStartDate.setText(promotions.getFrom_date().toString().substring(0, 10));
        holder.txtEndDate.setText(promotions.getTo_date().toString().substring(0, 10));
        Glide.with(context).load(promotions.getImage_url()).into(holder.image_Url);

        holder.btnReward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context = v.getContext();
                Intent intent = new Intent(context, PromotionAwardsList.class);
                intent.putExtra("promotion_id", "" + promotions.getId());
                context.startActivity(intent);
            }
        });

        holder.setItemClickListener(new PromotionActivity.ItemClickListener() {
            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                if (isLongClick) {
                    context = view.getContext();
                    Intent intent = new Intent(context, PromotionDetailsList.class);
                    //Lưu ý phải thêm vào dấu "" nếu muốn intent truyền được id
                    intent.putExtra("promotion_id", "" + promotions.getId());
                    context.startActivity(intent);
                } else {
//                    Toast.makeText(context, " " + promotionsList.get(position).getId(), Toast.LENGTH_SHORT).show();context = view.getContext();
                    context = view.getContext();
                    Intent intent = new Intent(context, PromotionDetailsList.class);
                    //Lưu ý phải thêm vào dấu "" nếu muốn intent truyền được id
                    intent.putExtra("promotion_id", "" + promotions.getId());
                    context.startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return promotionsList.size();
    }


}
