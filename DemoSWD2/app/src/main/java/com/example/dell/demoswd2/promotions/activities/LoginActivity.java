package com.example.dell.demoswd2.promotions.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import com.example.dell.demoswd2.R;
import com.example.dell.demoswd2.promotions.models.Customer;
import com.google.gson.Gson;

public class LoginActivity extends AppCompatActivity {
    private EditText txtCustomerId;
    private Button btnLogin;
    private RequestQueue requestQueue;
    private String customerId;
    private String customerName;
    private Float customerBalance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        txtCustomerId = findViewById(R.id.txtCustomerId);
        btnLogin = findViewById(R.id.btnLogin);
    }

    public void clickToLogin(View view) {

//        getCustomerAccount(txtCustomerId.getText().toString());

        requestQueue = Volley.newRequestQueue(this);

        //String url = "http://35.240.180.181:7717/api/accounts?ids=";
        String url = "http://productapi.unicode.edu.vn/customerapi/checkcustomer?TerminalID=1087&Phone=";

        url = url + txtCustomerId.getText().toString();

//        Toast.makeText(this, "URL:" + url, Toast.LENGTH_SHORT).show();
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
//                try {
////                    JSONArray jsonArray = response.getJSONArray("data");
////
////                    if (jsonArray.length() < 1) {
////
////                    } else {
////
////                        for (int i = 0; i < jsonArray.length(); i++) {
////                            JSONObject customerAccount = jsonArray.getJSONObject(i);
////
////                            customerId = customerAccount.getString("id");
////                            customerName = customerAccount.getString("name");
////                            customerBalance = (float) customerAccount.getDouble("balance");
////
////                            Intent intent = new Intent(getApplicationContext(), PromotionActivity.class);
////                            intent.putExtra("id", customerId);
////                            intent.putExtra("name", customerName);
////                            intent.putExtra("balance", customerBalance);
////
//////                        Toast.makeText(getApplicationContext(), "id: " + customerId + ", name: " + customerName + ", balance:" + customerBalance, Toast.LENGTH_SHORT).show();
////
////                            startActivity(intent);
////
////                        }
////                    }
////                } catch (JSONException e) {
////                    e.printStackTrace();
////                }

                try {
                    JSONObject jsonObject = response.getJSONObject("customer");

                            Gson gson = new Gson();
                            Customer customer = gson.fromJson(String.valueOf(jsonObject),Customer.class);

                            customerId = customer.getId();
                            customerName = customer.getName();
                            customerBalance = customer.getListMembership().get(0).getListAccount().get(0).getBalance();

                            Intent intent = new Intent(getApplicationContext(), PromotionActivity.class);
                            intent.putExtra("id", customerId);
                            intent.putExtra("name", customerName);
                            intent.putExtra("balance", customerBalance);

//                        Toast.makeText(getApplicationContext(), "id: " + customerId + ", name: " + customerName + ", balance:" + customerBalance, Toast.LENGTH_SHORT).show();

                            startActivity(intent);

                } catch (JSONException e) {
                    Toast.makeText(LoginActivity.this,"Không thể đăng nhập vào tài khoản, xin vui lòng thử lại!!!",Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        requestQueue.add(request);

    }
}
