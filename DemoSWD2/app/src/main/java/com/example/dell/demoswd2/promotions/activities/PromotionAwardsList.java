package com.example.dell.demoswd2.promotions.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.dell.demoswd2.R;
import com.example.dell.demoswd2.promotions.adapters.PromotionAwardsAdapter;
import com.example.dell.demoswd2.promotions.models.PromotionAwards;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class PromotionAwardsList extends AppCompatActivity {
    RecyclerView recyclerView;
    PromotionAwardsAdapter adapter;
    List<PromotionAwards> promotionAwardsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_4);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        promotionAwardsList = new ArrayList<>();

        Intent intent = getIntent();
        String promotion_id = intent.getStringExtra("promotion_id");

        String current_request = "http://35.240.180.181:8888/api/promotion_awards?active=true&of_promotion_id=" + promotion_id;

        Toast.makeText(this, "URL: " + current_request, Toast.LENGTH_SHORT).show();

        loadPromotionAwards(current_request);

    }

    public void loadPromotionAwards(String currentRequest) {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, currentRequest, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject obj = jsonArray.getJSONObject(i);

                        PromotionAwards promotionAwards = new PromotionAwards(
                                obj.getInt("id"),
                                obj.getString("code"),
                                obj.getBoolean("is_award_discount"),
                                obj.getBoolean("is_award_gift"),
                                obj.getString("discount_for_product_code"),
                                obj.getString("discount_amount"),
                                obj.getString("discount_percent"),
                                obj.getString("ship_discount"),
                                obj.getString("gift_for_product_code"),
                                obj.getString("gift_voucher_of_promotion_code"),
                                obj.getString("gift_voucher_quantity"),
                                obj.getString("gift_product_code"),
                                obj.getString("gift_product_quantity")
                        );

                        promotionAwardsList.add(promotionAwards);
                    }

                    adapter = new PromotionAwardsAdapter(promotionAwardsList, getApplicationContext());
                    recyclerView.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

}
