package com.example.dell.demoswd2.promotions.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Account implements Serializable {
    @SerializedName("Balance")
    private Float balance;

    public Account(Float balance) {
        this.balance = balance;
    }

    public Float getBalance() {
        return balance;
    }

    public void setBalance(Float balance) {
        this.balance = balance;
    }
}
